<html>
  <head>
    <title>Gallery 3 Installer</title>
    <link rel="stylesheet" type="text/css" href="install.css"/>
  </head>
  <body>
    <div id="outer">
      <img src="../modules/gallery/images/gallery.png" />
      <div id="inner">
        <h1> Success! </h1>
<p class="success">
  Your Gallery 3 install is complete!
</p>

<h2> Before you start using it... </h2>
<p>
  We've created an account for you to use:
  <br/>
  username: <b>admin</b>
  <br/>
  password: <b>e9d284</b>
  <br/>
  <br/>
  Save this information in a safe place, or change your admin password
  right away!
</p>

<h2> <a href="..">Start using Gallery</a> </h2>

      </div>
      <div id="footer">
        <p>
          <i>Did something go wrong? Try
          the <a href="http://codex.galleryproject.org/Gallery3:FAQ">FAQ</a>
          or ask in
          the <a href="http://galleryproject.org/forum">Gallery
          forums</a>.</i>
        </p>
      </div>
    </div>
  </body>
</html>
