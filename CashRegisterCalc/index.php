<?php

$ipas = glob('*.ipa');
$provisioningProfiles = glob('*.mobileprovision');
$plists = glob('*.plist');

$sr = stristr( $_SERVER['SCRIPT_URI'], '.php' ) === false ? 
    $_SERVER['SCRIPT_URI'] : dirname($_SERVER['SCRIPT_URI']) . '/';
$provisioningProfile = $sr . $provisioningProfiles[0];
$ipa = $sr . $ipas[0];
$itmsUrl = urlencode( $sr . 'index.php?plist=' . str_replace( '.plist', '', $plists[0] ) );


if ($_GET['plist']) {
    $plist = file_get_contents( dirname(__FILE__) 
        . DIRECTORY_SEPARATOR 
        . preg_replace( '/![A-Za-z0-9-_]/i', '', $_GET['plist']) . '.plist' );
    $plist = str_replace('_URL_', $ipa, $plist);
    header('content-type: application/xml');
    echo $plist;
    die();
}


?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Install iOS App</title>
<style type="text/css">

li {
    padding: 1em;
}

</style>
<!-- Art Direction Styles -->
<link href="../styles.css" rel="stylesheet" type="text/css" />

</head>
<body>

<div id="DivContainer">

<div id="Header"><div id="Logo"><h1></h1></div>

<div id="Contact">

<p>7135 South Mingo Rd
<br />Tulsa, Oklahoma 74133
<br />Phone: (918) 461-0228
<br /><a href="http://maps.google.com/maps?f=d38source=s_d38saddr=38daddr=7135+South+Mingo+Rd,+Tulsa,+OK+7413338hl=en38geocode=38mra=ls38sll=35.464169,-97.51069938sspn=0.008564,0.01931238ie=UTF838z=16" target="_blank">Get Directions</a></p>

</div></div>



<div id="Content" class="Content">

<ul>
    <li><a href="<? echo $provisioningProfile; ?>">Install Team Provisioning File</a></li>
    <li><a href="itms-services://?action=download-manifest&amp;url=http://www.kupcakzbakery.com/CashRegisterCalc/CashRegisterCalculator.plist">
         Install Application</a></li>
   </ul>
   </div>
   </div>
   </div>
</body>
</html>