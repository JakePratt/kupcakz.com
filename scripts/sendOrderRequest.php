<?php
if(isset($_POST['email'])) {
     
    // EDIT THE 2 LINES BELOW AS REQUIRED
    $email_to = "orders@KupcakzBakery.com";
    $email_subject = "KupcakzBakery.com Cupcake Order Request";
     
     
    function died($error) {
        // your error code can go here
        echo "We are very sorry, but there were error(s) found with the form you submitted. ";
        echo "These errors appear below.<br /><br />";
        echo $error."<br /><br />";
        echo "Please go back and fix these errors.<br /><br />";
        die();
    }
     
    // validation expected data exists
    if(!isset($_POST['first_name']) 	||
		!isset($_POST['last_name'])  	||
		!isset($_POST['email'])   		||
		!isset($_POST['pickupordelivery'])  ||
		!isset($_POST['areaCode']) ||
		!isset($_POST['phoneNumber']) ||
	
		!isset($_POST['VB']) ||
		!isset($_POST['CB']) ||
		!isset($_POST['PM']) ||
		!isset($_POST['CAR']) ||
		!isset($_POST['MB']) ||
		!isset($_POST['MAK']) ||
		!isset($_POST['PP']) ||
		!isset($_POST['CM']) ||
		!isset($_POST['RV']) ||
		!isset($_POST['TB']) ||
		!isset($_POST['PB']) ||
		!isset($_POST['BD']) ||
		!isset($_POST['DB']) ||
		!isset($_POST['CSC']) ||
		!isset($_POST['VSC']) ||
		!isset($_POST['VE']) ||
     	!isset($_POST['LD']) ||
     	!isset($_POST['BB']) ||
        !isset($_POST['COM']) ||
        !isset($_POST['OS']) ||
      
      
       !isset($_POST['time']) ||
       !isset($_POST['MM']) ||
       !isset($_POST['DD']) ||
       !isset($_POST['YYYY'])  			
       ) 
    {
        died('We are sorry, but there appears to be a problem with the form you submitted.');       
    }
     
    $first_name = $_POST['first_name']; // required
    $last_name = $_POST['last_name']; // required
    $email_from = $_POST['email']; // required
    $pickupordelivery = $_POST['pickupordelivery']; // required
    $address = $_POST['deliveryAddress']; // required
    $area = $_POST['areaCode']; // required
    $phone = $_POST['phoneNumber']; // required
    $MM = $_POST['MM']; // required
    $DD = $_POST['DD']; // required
    $YYYY = $_POST['YYYY']; // required
	$time = $_POST['time']; // required
     
     
     $VB = $_POST['VB']; // required
     $CB = $_POST['CB']; // required
     $PM = $_POST['PM']; // required
     $CAR = $_POST['CAR']; // required
     $MB = $_POST['MB']; // required
     $MAK = $_POST['MAK']; // required
     $RV = $_POST['RV']; // required
     $TB = $_POST['TB']; // required
     $PB = $_POST['PB']; // required
     $BD = $_POST['BD']; // required
     $DB = $_POST['DB']; // required
     $PP = $_POST['PP']; // required
     $CM = $_POST['CM']; // required
     $CSC = $_POST['CSC']; // required
     $VSC = $_POST['VSC']; // required
     $VE = $_POST['VE']; // required
     $LD = $_POST['LD']; // required
    $OS = $_POST['OS']; // required
     $BB = $_POST['BB']; // required
     $COM = $_POST['COM']; // required
     
    $error_message = "";
    $email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
  
  if(!preg_match($email_exp,$email_from)) {
    $error_message .= 'The Email Address you entered does not appear to be valid.<br />';
  }
  
  $string_exp = "/^[A-Za-z .'-]+$/";
  if(!preg_match($string_exp,$first_name) || strcmp("First Name",$first_name) == 0) {
    $error_message .= 'The First Name you entered does not appear to be valid.<br />';
  }
  if(!preg_match($string_exp,$last_name) || strcmp("Last Name",$last_name) == 0) {
    $error_message .= 'The Last Name you entered does not appear to be valid.<br />';
  }
 
 if(!preg_match("/^[0-9]{3}$/",$area)) {
   $error_message .= 'The Area Code you entered does not appear to be valid.<br />';
 }
 if(!preg_match("/^[0-9]{3}-?[0-9]{4}$/",$phone)) {
   $error_message .= 'The Phone Number you entered does not appear to be valid.<br />';
 }
  if(!is_numeric($MM)) {
    $error_message .= 'The Month you entered does not appear to be valid.<br />';
  }
  if(!is_numeric($DD)) {
    $error_message .= 'The Day you entered does not appear to be valid.<br />';
  }
  if(!is_numeric($YYYY)) {
    $error_message .= 'The Year you entered does not appear to be valid.<br />';
  }
  
  if(strcmp($pickupordelivery,'Delivery') == 0){
  	if(strlen($address) == 0 || strcmp($address,'Delivery in Tulsa Area Only.') == 0) {
  	  $error_message .= 'Please enter a delivery address.<br />';
  	}
  }
  
  if(strlen($error_message) > 0) {
    died($error_message);
  }
    $email_message = "An order has been placed on KupcakesBakery.com.\n\n";
     
    function clean_string($string) {
      $bad = array("content-type","bcc:","to:","cc:","href");
      return str_replace($bad,"",$string);
    }
     
    $email_message .= "First Name: 	".clean_string($first_name)."\n";
    $email_message .= "Last Name: 	".clean_string($last_name)."\n";
    $email_message .= "Email: 		".clean_string($email_from)."\n";
    $email_message .= "Phone: 		".clean_string($area)."-".clean_string($phone)."\n\n";
    $email_message .= "Date: 		".clean_string($MM)."-".clean_string($DD)."-".clean_string($YYYY)."\n";
    $email_message .= "Time: 		".clean_string($time)."\n\n";
    $email_message .= "Delivery or Pickup: ".clean_string($pickupordelivery)."\n";
    if(strcmp($pickupordelivery,'Delivery') == 0){
    	$email_message .= "Address: 		".clean_string($address)."\n\n";
    }
   
	$email_message .= "Cupcake of the Month: ".clean_string($COM)."\n";
    $email_message .= "Vanilla Beenie: ".clean_string($VB)."\n";
	$email_message .= "Chocolate Beenie: ".clean_string($CB)."\n";
	$email_message .= "Peppermint Paddy: ".clean_string($PM)."\n";
	$email_message .= "24 Carrot: ".clean_string($CAR)."\n";
	$email_message .= "Morning Buzz: ".clean_string($MB)."\n";
	$email_message .= "Triple Chocolate: ".clean_string($MAK)."\n";
	$email_message .= "Pretty in Pink: ".clean_string($PP)."\n";
	$email_message .= "Cheeky Monkey: ".clean_string($CM)."\n";
	$email_message .= "Red Velvet: ".clean_string($RV)."\n";
	$email_message .= "Tropical Bliss: ".clean_string($TB)."\n";
	$email_message .= "Peanut Buttercup: ".clean_string($PB)."\n";
	$email_message .= "Boston Dreamer: ".clean_string($BD)."\n";
	$email_message .= "Dirty Blond: ".clean_string($DB)."\n";
	$email_message .= "Chocolate Salted Caramel: ".clean_string($CSC)."\n";
	$email_message .= "Vanilla Salted Caramel: ".clean_string($VSC)."\n";
	$email_message .= "Velvet Elvis: ".clean_string($VE)."\n";
	$email_message .= "Lemon Drop: ".clean_string($LD)."\n";
	$email_message .= "Brown Bettie: ".clean_string($BB)."\n";
	$email_message .= "Old School: ".clean_string($OS)."\n";
	  
$email_OurFrom = "orders@kupcakzbakery.com:"; // required
 
     
// create email headers
$headers = 'From: '.$email_OurFrom."\r\n".
'Reply-To: '.$email_from."\r\n" .
'X-Mailer: PHP/' . phpversion();
@mail($email_to, $email_subject, $email_message, $headers);  

header("Location: http://kupcakzbakery.com/ordercomplete.php.html");
/* Make sure that code below does not get executed when we redirect. */

exit;

?>
 
<!-- include your own success html here -->

<?php
}
?>